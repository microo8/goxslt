package xslt

import (
	"io/ioutil"
	"testing"
)

func TestXslt(t *testing.T) {
	styleSheet, err := ioutil.ReadFile("general_agenda.xsl")
	if err != nil {
		t.Fatal(err)
	}
	xml, err := ioutil.ReadFile("form.xml")
	if err != nil {
		t.Fatal(err)
	}
	result := ApplyStyleSheet(string(styleSheet), string(xml))
	ioutil.WriteFile("/tmp/general_agenda.html", []byte(result), 0644)
}
