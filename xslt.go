package xslt

/*
#cgo CFLAGS: -I/usr/include/libxml2
#cgo linux LDFLAGS: -L/usr/lib -l xml2 -l xslt
#cgo windows LDFLAGS: -l xml2 -l xslt

#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
*/
import "C"
import "unsafe"

//ApplyStyleSheet ...
func ApplyStyleSheet(xslt, xml string) string {
	xlstChars := C.CString(xslt)
	defer C.free(unsafe.Pointer(xlstChars))
	docChars := C.CString(xml)
	defer C.free(unsafe.Pointer(docChars))

	C.xmlSubstituteEntitiesDefault(1)
	styleSheetDoc := C.xmlReadMemory(xlstChars, C.int(len(xslt)), nil, nil, 0)
	styleSheet := C.xsltParseStylesheetDoc(styleSheetDoc)
	defer C.xsltFreeStylesheet(styleSheet)
	doc := C.xmlReadMemory(docChars, C.int(len(xml)), nil, nil, 0)
	defer C.xmlFreeDoc(doc)
	ctx := C.xsltNewTransformContext(styleSheet, doc)
	defer C.xsltFreeTransformContext(ctx)
	C.xsltSetCtxtParseOptions(ctx, C.XSLT_PARSE_OPTIONS)

	res := C.xsltApplyStylesheetUser(styleSheet, doc, nil, nil, nil, ctx)
	defer C.xmlFreeDoc(res)
	var buff *C.uchar
	var buffSize C.int
	C.xsltSaveResultToString(&buff, &buffSize, res, styleSheet)
	defer C.free(unsafe.Pointer(buff))
	return C.GoStringN((*C.char)(unsafe.Pointer(buff)), buffSize)
}
